package com.confspot

import org.codehaus.groovy.grails.plugins.springsecurity.GormUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.core.GrantedAuthority
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser

class TwitterUserDetailsService extends GormUserDetailsService {

    /**
     * {@inheritDoc}
     * @see org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserDetailsService#loadUserByUsername(
     * 	java.lang.String, boolean)
     */
    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {
        User.withTransaction { status ->
            def user = User.findByUsername(username)
            if (!user) {
                log.warn "User not found: $username"
                throw new UsernameNotFoundException('User not found', username)
            }
            Collection<GrantedAuthority> authorities = loadAuthorities(user, username, loadRoles)
            new GrailsUser(user.username, 'N/A', user.enabled, !user.accountExpired, !user.passwordExpired,
                    !user.accountLocked, authorities, user.id)
        }
    }

}
