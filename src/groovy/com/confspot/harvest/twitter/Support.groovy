package com.confspot.harvest.twitter

import groovy.util.logging.Log4j

// Para twitter4j
import twitter4j.*
import twitter4j.conf.ConfigurationBuilder

import org.codehaus.groovy.grails.commons.GrailsApplication;
import org.json.simple.*

@Log4j
public class Support {
	
	// Not injected by grails
	GrailsApplication grailsApplication
	
	// Para la paginación por defecto
	public final static Paging DEFAULT_PAGING = new Paging(1, 100) // Página número 1, 100 resultados

	private def twitter
	boolean connected
	
	/**
	 * Esta closure realiza una conexión interactiva a Twitter, solicitando al usuario el pin para la out of band
	 * authentication. Antes se le mostrará al usuario por stdout la url que solicitar en el navegador para que
	 * Twitter le dé el pin.
	 *
	 * Si todo va bien y se realiza la conexión el flag connected se establecerá a true y en la variable twitter
	 * se almacenará una referencia a un objeto que permitirá el acceso a la funcionalidad principal de twitter.
	 */
	def connectTwitterInteractive() {
		def connection, pin
		def accessToken
	
		twitter = new TwitterFactory().getInstance()
		twitter.setOAuthConsumer(getConsumerKey(), getConsumerSecret())
		def requestToken = twitter.getOAuthRequestToken()
		def authorizedUrl = requestToken.authorizationURL
	
		System.in.withReader {
			println "URL (copia y pega en el navegador, autoriza y escribe el pin que te den en Twitter): ${authorizedUrl}"
			print 'PIN: '
			pin = it.readLine() // Leemos el pin que nos meta el usuario por stdin
		}

		try {
			accessToken = twitter.getOAuthAccessToken(requestToken, pin) 
			connected = twitter.verifyCredentials() != null
			log.debug("Access token: ${accessToken}")
			log.debug("Access token: ${accessToken.token}")
			log.debug("Access token secret: ${accessToken.tokenSecret}")
		} catch(Exception e) {
			log.error("Error en la conexión con Twitter: ${e}")
			connected = false
		}

		return twitter
	}

	/**
	 * Esta closure realiza una conexión no interactiva a Twitter, sin requerir para nada entrada del usuario. Para
	 * recupera un access token almacenado en la base de datos y previamente recuperado con la conexión interactiva.
	 * Consigue una conexión no interactiva mediante twitter4j.
	 */
 	def connectTwitterNotInteractive(String accessToken, String accessTokenSecret) {
		ConfigurationBuilder cb = new ConfigurationBuilder()
		cb.setDebugEnabled(true).setOAuthConsumerKey(getConsumerKey())
			.setOAuthConsumerSecret(getConsumerSecret())
			.setOAuthAccessToken(accessToken)
			.setOAuthAccessTokenSecret(accessTokenSecret)

		TwitterFactory tf = new TwitterFactory(cb.build())
		twitter = tf.getInstance()
	
		try {
			connected = twitter.verifyCredentials() != null
		} catch(Exception e) {
			connected = false
			println e.cause
			println "$e.message\n"
		}

		return twitter
	}

	/**
	 * Conecta todas las conexiones configuradas a Twitter y devuelve una lista con objetos para
	 * acceder a ellos.
	 */
	List connectBatchTwitter() {
		List results = []
		def twitter, accessToken, accessTokenSecret

		getAccessTokens().size().times { time ->
			accessToken = getAccessTokens()[time]
			accessTokenSecret = getAccessTokenSecrets()[time]
			twitter = connectTwitterNotInteractive(accessToken, accessTokenSecret)
			if(connected) {
				results << twitter
			} else {
				log.warn("No ha sido posible conectar con Twitter empleando ${accessToken} -- ${accessTokenSecret}")
			}
		}
		return results
	}

	/**
	 * Devuelve el id en Twitter del usuario cuyo screen name se pasa como parámetro.
	 */
	String getUserId(String screenName) {
		def user = twitter.showUser(screenName)
		return user ? user.id.toString() : ''
	}

	/**
	 * Imprime los user ids asociados a los usuarios de twitter cuyos screen name se pasan como parámetro.
	 */
	def printTwitterUserIds(screenNames) {
		def user

		screenNames.each{ screenName ->
			user = twitter.showUser(screenName)
			log.info("Screen name: ${screenName}; User id: " + (user ? user.id : "El usuario ${screenName} no ha sido encontrado"))
		}
	}

	/**
	 * Devuelve la lista de ids de usuarios a los que sigue el usuario cuyo id se pasa como parámetro.
	 */
	def getFriends(userId) {
		def cursor = -1
		def allFriends = []
		def result, friends

		try {
			friends = twitter.getFriendsIDs(userId, cursor)
			allFriends << friends

			while(friends.hasNext()) {
				friends = twitter.getFriendsIDs(userId, friends.nextCursor)
				allFriends << friends
			}

			result = allFriends.collect{it.IDs}.flatten()
		} catch (Exception e) {
			log.error("Error al recuperar los amigos de ${userId}: ${e}")
			result = []
		}

		//println "Friends de ${userId}: ${result?.size()}"

		return result
	}


	/**
	 * Devuelve la lista de ids de usuarios que siguen al usuario cuyo id se pasa como parámetro.
	 */
	def getFollowers(userId) {
		def cursor = -1
		def allFollowers = []
		def result, followers

		try {
			followers = twitter.getFollowersIDs(userId, cursor)
			allFollowers << followers

			while(followers.hasNext()) {
				followers = twitter.getFollowersIDs(userId, followers.nextCursor)
				allFollowers << followers
			}

			result = allFollowers.collect{it.IDs}.flatten()
		} catch (Exception e) {
			log.error("Error al recuperar los followers de ${userId}: ${e}")
			result = []
		}

		//println "Followers de ${userId}: ${result?.size()}"

		return result
	}

	/**
	 * Devuelve la lista de ids de las listas en las que está incluído el usuario cuyo id se pasa como parámetro.
	 */
	def getListMemberships(userId) {
		def cursor = -1
		def allLists = []
		def result, lists

		try {
			lists = twitter.getUserListMemberships(userId, cursor)
			allLists << lists

			while(lists.hasNext()) {
				lists = twitter.getUserListMemberships(userId, lists.nextCursor)
				allLists << lists.toArray()
			}

			result = allLists.flatten().collect{it.id}
		} catch (Exception e) {
			log.error("Error al recuperar los followers de ${userId}: ${e}")
			result = []
		}

		//println "Listas a las que pertenece de ${userId}: ${result?.size()}"

		return result
	}
	
	private String getConsumerKey() {
		grailsApplication.config.twitter.key
	}
	
	private String getConsumerSecret() {
		grailsApplication.config.twitter.secret
	}
	
	private List<String> getAccessTokens() {
		grailsApplication.config.twitter.accessTokens
	}
	
	private List<String> getAccessTokenSecrets() {
		grailsApplication.config.twitter.accessTokenSecrets
	}
}
