package com.confspot.harvest.twitter

import org.codehaus.groovy.grails.commons.GrailsApplication;

class TwitterCredentials {
	List accessTokens = []
	List accessTokenSecrets = []
	String consumerKey
	String consumerSecret

	public static TwitterCredentials loadTwitterCredentials(GrailsApplication grailsApplication) {
		TwitterCredentials result = new TwitterCredentials()

		result.consumerKey = grailsApplication.config.grails.plugins.springsecurity.twitter.app.consumerKey
		result.consumerSecret = grailsApplication.config.grails.plugins.springsecurity.twitter.app.consumerKeySecret
		result.accessTokens = grailsApplication.config.twitter.accessTokens
		result.accessTokenSecrets = grailsApplication.config.twitter.accessTokenSecrets

		return result
	}
}
