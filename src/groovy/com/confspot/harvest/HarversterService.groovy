package com.confspot.harvest

import com.confspot.Conference
import com.confspot.Item

abstract class HarvesterService {

    public harvest() {

    	// TODO: Decide the time-window to get events
		def confs = Conference.list()
		confs.each { conf ->

			// Get all external items
			def externalItems = this.getExternalItems(conf.hashtag)

			externalItems.each { externalItem ->
				try {
					// Check if the element is already stored
					def element = findElement(externalItem)
	
					if (!element) {
						element = createElement(externalItem)
				 		element.addToConferences(conf)
						conf.addToItems(element)
						if(element.validate()) {
							element.save()
						} else {
							conf.removeFromItems(element)
							element.errors.each{log.error(it)}
						}
					} else {
						updateElement(element, externalItem)
						element.save()
					}
				} catch(Exception e) {
					log.error(e.message ?: e.cause)
				}
			}
		}
    }

    protected abstract List<Item> getExternalItems(String query);
    protected abstract Item findElement(Map externalItem);
    protected abstract Item createElement(Map params);
    protected Item updateElement(Item item, Map params) {
    	// Do nothing
    	return item
    }
}