var map
function getMap(userLat, userLng) {
	map = new GMaps({
	        div: '#map',
	        zoom: 12,
	        lat: userLat?userLat:40.437976,
	        lng: userLng?userLng:-3.687327
	    });
	if(userLat != null && userLng != null){
		map.addMarker({
			lat: userLat,
			lng: userLng,
			title: 'You are here'
		});
	}
	
	//Add a button to change the address
//	map.addControl({
//		position: 'top_right',
//		content: 'I\'m not here!',
//		style: {
//			margin: '5px',
//			padding: '1px 6px',
//			border: 'solid 1px #717B87',
//			background: '#fff'
//		},
//		events: {
//			click: function(){
//				$('#modal').modal();
//			}
//		}
//	});
}

function locate() {
	$('#modal').modal('hide');
    var address = $('#address').val();
    if(address != null && address.replace(/^\s+/g,'').replace(/\s+$/g,'').length > 0){
        GMaps.geocode({
        	address: address,
            callback: function(results, status){
                if (status == 'OK') {
                	map.removeMarkers();
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        title: 'You are here'
                    });
                }
            }
        });
    }
    return false;
}

function situate(){
	var lat = $('#userLat').val();
	var lng = $('#userLng').val();
	getMap(lat, lng);
}

function geoSituate(data,textStatus){
	$('#userLat').val(data[0].lat);
	$('#userLng').val(data[0].lng);
	getMap(data[0].lat, data[0].lng);
}

$(document).ready(function(){
	situate();
});
