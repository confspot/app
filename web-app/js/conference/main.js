function infinite_scroll() {
	$("#infinite-scroll").appear(function(e) {
	    e.preventDefault();

		var self = $(this);

	    var nItems = $('.span12 .span4').length;
	    var url = self.data('url');

		$.ajax({
			url: url,
			data: {nItems: nItems },
			dataType: "html"
		}).done(function(html) {
			$('.span12').append(html);
			self.remove();
			$('#container').masonry('reload');
			infinite_scroll();
		});
	});
	
}

function masonry(){
	$('#container').masonry({
		// options
		itemSelector : '.item'
	});
};

