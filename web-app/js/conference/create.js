function locateConference() {
    var address = $('#localityNameLoc').val() +', '+$('#countryNameLoc').val();
    if(address != null && address.replace(/^\s+/g,'').replace(/\s+$/g,'').length > 0){
        GMaps.geocode({
        	address: address,
            callback: function(results, status){
                if (status == 'OK') {
                	map.removeMarkers();
                    var latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                    map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        title: 'The conference is here'
                    });
                    $('#lat').val(latlng.lat())
                    $('#lng').val(latlng.lng())
                    $('#localityName').val($('#localityNameLoc').val())
                    $('#countryName').val($('#countryNameLoc').val())
                }else{
                	$('#localityNameLoc').val('').addClass('error')
                	$('#countryNameLoc').val('').addClass('error')
                }
            }
        });
    }
    return false;
}

$(document).ready(function(){
	if($('#localityNameLoc').val() != '' && $('#countryNameLoc').val() != '') {
		locateConference();
	}
	$( "#begin" ).datepicker({
	    defaultDate: "+1w",
	    changeMonth: true,
	    "dateFormat": $("#formatDate").val(),
	    onClose: function( selectedDate ) {
	        $( "#end" ).datepicker( "option", "minDate", selectedDate );
	    }
	});
	$( "#end" ).datepicker({
		changeMonth: true,
		"dateFormat": $("#formatDate").val(),
	    onClose: function( selectedDate ) {
	        $( "#begin" ).datepicker( "option", "maxDate", selectedDate );
	    }
	});
});