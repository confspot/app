var Dashboard = {};

Dashboard.fetchStream = function(url, offset, divName,callBack) {
	$('#'+divName+'Loader').fadeIn();
	$.ajax({
		url:url,
		data: {offset: offset },
		dataType: "html"
	}).done(function(result) {
		$('#'+divName+'Loader').fadeOut();
		$('#'+divName).append(result);
		if(callBack) {
			callBack.call();
		}
	});
}

Dashboard.updateActivityStream = function(url) {
	$('#activityContentLoader').fadeIn();
	//We'll be referencing lastShowedItem var, which is is defined @dashboard.gsp
	$.ajax({
		url:url,
		data: {objectId: lastShowedItem },
		dataType: "html"
	}).done(function(result) {
		$('#activityContentLoader').fadeOut();
		if(result) {
			$('#activityContent').html(result);
		}
	});
}

Dashboard.updateHallOfFame = function(url){
	$.ajax({
		url:url,
		dataType: "html"
	}).done(function(result) {
		if(result) {
			$('#hallOfFame').html(result);
		}
	});
}
