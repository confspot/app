<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main" />
	<title><g:message code="layout.main.title" /></title>
	<geolocation:resources/>
	<r:require module="shareGeolocation"/>
</head>
<body>

	<div id="featureCarousel" class="carousel slide">
	    <!-- Carousel items -->
	    <div class="carousel-inner">
	        <g:each in="${conferences}" var="conference" status="i">
	            <div class="item ${!i ? 'active':''}">
	                <g:img dir="images/conference" file="${conference.photo}.jpg" alt="${conference.title.encodeAsHTML()}" height="600"/>
	                <div class="carousel-caption clearfix">
	                    <p>
	                        <strong><g:message code="conference.begin" />:</strong> <g:formatDate format="${message(code:'default.date.format')}" date="${conference.begin}"/>
	                    | <strong><g:message code="conference.end" />:</strong> <g:formatDate format="${message(code:'default.date.format')}" date="${conference.end}"/>
	                    | ${conference.localityName.encodeAsHTML()} (${conference.countryName.encodeAsHTML()})
	                    </p>
	                    <h1 class="pull-left">${conference.title.encodeAsHTML()}</h1>
	                    <p>
	                        <g:link class="btn btn-primary btn-large pull-right" mapping="showConference" params="[slug:conference.slug]">
	                            <g:message code="home.showMore" />
	                        </g:link>
	                    </p>
	                </div>
	            </div>
	        </g:each>
	    </div>
	    <!-- Carousel nav -->
	    <a class="carousel-control left" href="#featureCarousel" data-slide="prev">
	        <i class="icon-chevron-left"></i>
	    </a>
	    <a class="carousel-control right" href="#featureCarousel" data-slide="next">
	        <i class="icon-chevron-right"></i>
	    </a>
	</div>
	
	<div class="hero-unit" style="text-align: center">
	    <h1><g:message code="home.index.welcome" args="[r.img(file:'logo.png')]" /></h1>
	    <p style="font-size: 1.5em"><g:message code="home.index.slogan" /></p>
	</div>
	
	<div class="container">
	    <section id="why">
	        <div class="row">
	            <div class="span4">
	                <h3><span>#</span></i> <g:message code="home.index.feature1.title" /></h3>
	                <g:message code="home.index.feature1.body" />
	            </div>
	            <div class="span4">
	                <h3><i class="icon-search"></i> <g:message code="home.index.feature2.title" /></h3>
	                <g:message code="home.index.feature2.body" />
	            </div>
	            <div class="span4">
	                <h3><i class="icon-ok"></i> <g:message code="home.index.feature3.title" /></h3>
	                <g:message code="home.index.feature3.body" />
	            </div>
	        </div>
	    </section>
	</div>
	
	<g:if test="${!session.position}">
		<geolocation:locateMe />
	</g:if>
	<g:hiddenField name="userLat" value="${session.position?.coords?.latitude}" />
	<g:hiddenField name="userLng" value="${session.position?.coords?.longitude}" />

</body>
</html>
