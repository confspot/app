<div id="listConferences">
	<g:if test="${!conferences}">
		<bootstrap:alert title="" type="alert">
			<g:message code="conference.myConferences.list.empty"/>
			<g:link mapping="createConference"><g:message code="layout.signUpYourConference"/></g:link>
		</bootstrap:alert>
	</g:if>
	<g:else>
		<table class="table table-striped">
			<thead>
				<tr>
					<th><g:message code="conference.myConferences.list.hashtag"/></th>
					<th><g:message code="conference.myConferences.list.title"/></th>
					<th><g:message code="conference.myConferences.list.begin"/></th>
					<th><g:message code="conference.myConferences.list.end"/></th>
					<th><g:message code="conference.myConferences.list.urlSite"/></th>
					<th><g:message code="conference.myConferences.actions"/></th>
				</tr>
			</thead>
			<tbody>
					<g:each in="${conferences}" var="conf">
						<tr>
							<td><g:link mapping="searchConferences" params="[text:conf.hashtag]">${conf.hashtag}</g:link></td>
							<td>${conf.title}</td>
							<td><g:formatDate date="${conf.begin}" format="MM/dd/yyyy"/></td>
							<td><g:formatDate date="${conf.end}" format="MM/dd/yyyy"/></td>
							<td><g:link url="${conf.urlSite}">${conf.urlSite}</g:link></td>
							<td>
								<g:link class="btn btn-small" mapping="showConference" params="[slug:conf.slug]"><i class="icon-large icon-eye-open" title="${message(code:'conference.actions.show')}"></i></g:link>
								<g:link class="btn btn-small" mapping="editConference" params="[slug:conf.slug]"><i class="icon-large icon-edit" title="${message(code:'conference.actions.edit')}"></i></g:link>
								<g:link class="btn btn-small" mapping="deleteConference" params="[slug:conf.slug]"><i class="icon-large icon-remove" title="${message(code:'conference.actions.delete')}"></i></g:link>
							</td>
						</tr>
					</g:each>
			</tbody>
		</table>
	</g:else>
</div>