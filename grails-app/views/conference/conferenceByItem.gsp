<%@ page defaultCodec="HTML" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="layout.main.title" /></title>
		<r:require modules="conference, masonry"/>
	</head>
	<body>
		<div class="container">
			<div class="row-fluid">
				
				<%-- Conference info --%>
				<g:render template="/conference/conferenceDetail" model="[conference:conference]" />

				<br/><br/>

				<%-- Items --%>
				
				<h3><g:img class="dashboardIcon" file="${type.capitalize()}-icon.png"/> <g:message code="conference.type.${type}"/></h3>
				
				<div id="container" class="span12">	
					<g:if test="${items.size() > 0}">
						<g:render template="/conference/items" model="[items:items, conference:conference, type:type]" />
					</g:if>
					<g:else>
						<bootstrap:alert type="warning" title="${message(code: 'conference.noResults.sorry')}">
							<g:message code="conference.noResults"/>
						</bootstrap:alert>
					</g:else>
				</div>

			</div>
		</div>

		<r:script>
   			infinite_scroll();
   			masonry();
		</r:script>
	</body>
</html>