<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="conference.myConferences.title"/></title>
		<r:require module="myconferences"/>
	</head>
	<body>
        <br/>
        <div class="container">
            <div class="page-header">
			    <h1><g:message code="conference.myConferences.title"/></h1>
            </div>
			<g:render template="list" model="[conference:conference]"/>
		</div>
	</body>
</html>