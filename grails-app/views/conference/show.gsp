<%@ page defaultCodec="HTML" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>${conference.title}</title>
		<r:require module="conference"/>
	</head>
	<body>
		<div class="container">
			<div class="row-fluid">
				<%-- Conference info --%>
				<g:render template="/conference/conferenceDetail" conference="${conference}" />
				<g:include action="dashboard" controller="dashboard" id="${conference.id}" model="[items:items]"/>
			</div>
		</div>

		<r:script>
			$('#search-box').attr('action', '${createLink(mapping:"searchInConference", params:[slug:conference.slug])}');
		</r:script>
	</body>
</html>
