<%@ page defaultCodec="HTML" %>

<g:each in="${items}" var="item">
	<div class="item span4 marginLeft0 ">
		<aside id="sidebar" class="well marginRight10 marginLeft0">
			<render:item item="${item}"/>
		</aside>
	</div>
</g:each>

<g:if test="${items.size() > 0}">
	<div id="infinite-scroll" data-url="${createLink(mapping:'conferenceDetailItem', params:[slug:conference.slug, type:type])}">
	</div>
</g:if>