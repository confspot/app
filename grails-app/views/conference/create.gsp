<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="layout.signUpYourConference" /> </title>
		<r:require modules="createConference, geolocation"/>
	</head>
	<body>
        <br/>
        <div class="container">
            <g:render template="/templates/message" />

            <section id="conference">
				<h1 class="section-header"><g:message code="conference.create" /></h1>
				<div class="row-fluid">
					<h2><g:message code="conference.form.geolocation.leyend"/></h2>
					<blockquote>
						<p><g:message code="conference.create.locationExplication" /></p>
					</blockquote>
					<form id="conferenceGeolocForm" class="form-horizontal">
						<fieldset>
							<div class="control-group span4">
								<label class="control-label" for="localityNameLoc"><g:message code="conference.localityName.label"/></label>
								<div class="controls">
									<g:textField name="localityNameLoc" value="${conference?.localityName?:''}"/>
								</div>
							</div>
							<div class="control-group span4">
								<label class="control-label" for="countryNameLoc"><g:message code="conference.countryName.label"/></label>
								<div class="controls">
									<g:textField name="countryNameLoc" value="${conference?.countryName?:''}"/>
								</div>
							</div>
							<div class="control-group span4">
								<div class="controls">
		    						<a href="#" class="btn btn-primary" onclick="locateConference()">
		    							<i class="icon-search icon-white"></i>
		    							<g:message code="geolocation.search" />
		    						</a>
								</div>
							</div>
						</fieldset>
					</form>
					<div id="map" class="map span12"></div>
				</div>
				<div class="row-fluid">
					<h2><g:message code="conference.form.info.leyend"/></h2>
					<blockquote>
						<p><g:message code="conference.create.infoExplication" /></p>
					</blockquote>
					<g:form name="conferenceForm" action="save" class="form-horizontal">
						<g:hiddenField name="id" value="${conference?.id}"/>
						<fieldset>
							<div class="row-fluid">
								<div class="control-group span9">
									<label class="control-label" for="hashtag"><g:message code="conference.hashtag.label"/></label>
									<div class="controls">
										<div class="input-prepend">
			                                <span class="add-on">#</span>
			                                <g:textField name="hashtag" value="${conference?.hashtag?:''}"/>
			                            </div>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span9">
									<label class="control-label" for="title"><g:message code="conference.title.label"/></label>
									<div class="controls">
										<g:textField class="input-xxlarge" name="title" value="${conference?.title?:''}"/>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span9">
									<label class="control-label" for="urlSite"><g:message code="conference.urlSite.label"/></label>
									<div class="controls">
										<g:textField class="input-xxlarge" name="urlSite" value="${conference?.urlSite?:''}"/>
									</div>
								</div>
							</div>
							<g:hiddenField id="formatDate" name="formatDate" value="${message(code:'locale.format.date.javascript')}"/>
							<div class="row-fluid">
								<div class="control-group span4">
									<label class="control-label" for="begin"><g:message code="conference.begin.label"/></label>
									<div class="controls">
										<g:textField class="datePicker" id="begin" name="begin" value="${g.formatDate(format:message(code:'locale.format.date.java'),date:conference?.begin)}"/>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="control-group span4">
									<label class="control-label" for="end"><g:message code="conference.end.label"/></label>
									<div class="controls">
										<g:textField class="datePicker" id="end" name="end" value="${g.formatDate(format:message(code:'locale.format.date.java'),date:conference?.end)}"/>
									</div>
								</div>
							</div>
						</fieldset>
						<g:hiddenField id="lat" name="lat" value="${conference?.lat ?: ''}"/>
						<g:hiddenField id="lng" name="lng" value="${conference?.lng ?: ''}"/>
						<g:hiddenField id="localityName" name="localityName" value="${conference?.localityName ?: ''}"/>
						<g:hiddenField id="countryName" name="countryName" value="${conference?.countryName ?: ''}"/>
						<div class="control-group">
	    					<div class="controls">
	    						<g:if test="${conference?.id}">
									<g:submitButton name="conferenceUpdateButton" value="${g.message(code:'conference.form.update.button')}" class="btn btn-primary"></g:submitButton>
								</g:if>
								<g:else>
									<g:submitButton name="conferenceSaveButton" value="${g.message(code:'conference.form.save.button')}" class="btn btn-primary"></g:submitButton>
									
								</g:else>
							</div>
						</div>
					</g:form>
				</div>
			</section>
		</div>
	</body>
</html>
