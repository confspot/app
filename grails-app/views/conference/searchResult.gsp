<%@ page defaultCodec="HTML" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to cOnfSpoT!</title>
		<r:require module="conference"/>
	</head>
	<body>

		<content tag="search">
			<g:form class="navbar-search pull-left" mapping="searchConferences" method="get">
				<input type="text" name="text" class="search-query span2" placeholder="${message(code:'conference.search')}">
			</g:form>
		</content>

		<div class="container">
			<div class="row-fluid">

				<g:if test="${conferences.size() > 0}">
					<g:each in="${conferences}" var="conference">
						<%-- Conference info --%>
						<g:render template="/conference/conferenceDetail" model="[conference:conference]" />
					</g:each>
				</g:if>
				<g:else>
					<bootstrap:alert type="info" header="${message(code:'conference.search.notFound.title')}">
						<br/><g:message code="conference.search.notFound.body" />
					</bootstrap:alert>
				</g:else>

			</div>
		</div>
	</body>
</html>
