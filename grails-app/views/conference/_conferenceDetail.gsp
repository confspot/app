<%@ page defaultCodec="HTML" %>

<div class="page-header">
	<g:link mapping="showConference" params="[slug:conference.slug]">
	    <h1>${conference.title}</h1>
	</g:link>
</div>

<div class="span5">
	<dl class="dl-horizontal ">
		<dt>
			<g:message code="conference.begin"/>
		</dt>
		<dd>
			<g:formatDate format="${message(code:'default.date.format')}" date="${conference.begin}"/> 
		</dd>
		<dt>
			<g:message code="conference.end"/>
		</dt>
		<dd>
			<g:formatDate format="${message(code:'default.date.format')}" date="${conference.end}"/>
		</dd>
		<dt>
			<g:message code="conference.localityName"/>
		</dt>
		<dd>
			${conference.localityName}
		</dd>
		<dt>
			<g:message code="conference.countryName"/>
		</dt>
		<dd>
			${conference.countryName}
		</dd>
		<dt>
			<g:message code="conference.urlSite"/>
		</dt>
		<dd>
			 <a href="${conference.urlSite}">${conference.urlSite}</a>
		</dd>
		<dt>
			<g:message code="conference.organizer"/>
		</dt>
		<dd>
			<a href="https://twitter.com/${conference.organizer.username}" target="_blank"><g:img class="icon" file="Tweet-icon.png"/> ${conference.organizer.username}</a>
		</dd>
		<dt>
			<g:message code="conference.hashtag"/>
		</dt>
		<dd>
			<a href="https://twitter.com/search?q=%23${conference.hashtag}&src=hash" target="_blank"> #${conference.hashtag}</a>
		</dd>
	</dl>
</div>
<div class="span7 marginLeft0">
	<g:img class="conferenceImage" file="conference/${conference.photo}.jpg"/>
</div>