<geolocation:resources/>
<r:require module="geolocation"/>
<g:if test="${!session.position}">
	<geolocation:locateMe />
</g:if>
<bootstrap:alert type="info" header="${message(code: 'geolocation.addressNotCorrect.header')}">
	<g:message code="geolocation.addressNotCorrect"/>
	<button onclick="$('#modal').modal()" class="btn btn-inverse " ><g:message code="geolocation.locate"/></button>
</bootstrap:alert>
<div id="map" class="map"></div>
<g:hiddenField name="userLat" value="${session.position?.coords?.latitude}" />
<g:hiddenField name="userLng" value="${session.position?.coords?.longitude}" />
<g:render template="../templates/searchLocation"/>
