<g:hiddenField name="searchLocation"
	value="${resource(dir:'templates', file:'_searchLocation.gsp', absolute: true)}" />

<div id="modal" class="modal fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
		<h3 id="myModalLabel">
			<g:message code="geolocation.yourAddress" />
		</h3>
	</div>
	<div class="modal-body locationSearchBox">
		<g:textField name="address" type="text" class="search-query"
			placeholder="${message(code: 'geolocation.search')}" />
		<button class="btn btn-primary" onclick="locate()">
			<g:message code="geolocation.search" />
		</button>
	</div>
	<div class="modal-footer"></div>
</div>