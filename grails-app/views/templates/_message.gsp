<g:if test="${flash.message}">
    <br/>
	<bootstrap:alert type="${flash.message.type}" title="${flash.message.title}">
		${flash.message.body}
	</bootstrap:alert>
</g:if>