<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="500.title"/></title>
</head>
<body>

    <br/>
    <div class="container">
        <div class="page-header">
            <h1><g:message code="500.title"/></h1>
        </div>
        <div class="row" style="text-align: center">
            <r:img file="error_500.jpg" /> <br/><br/>
            <a href="#" class="btn btn-primary btn-large" data-original-title="btn btn-primary btn-large" onclick="javascript:history.go(-1)">
                <g:message code="500.button" />
            </a>
        </div>
    </div>

</body>
</html>