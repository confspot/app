<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><g:message code="404.title" /></title>
    <link href="${resource(file:'/css/404.css')}" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <h1><g:message code="404.notFound" /> <span>:(</span></h1>
    <g:message code="404.explanation" />
    <script>
        var GOOG_FIXURL_LANG = (navigator.language || '').slice(0,2),GOOG_FIXURL_SITE = location.host;
    </script>
    <script src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js"></script>
</div>
</body>
</html>
