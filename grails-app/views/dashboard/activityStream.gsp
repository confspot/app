<g:each var="item" in="${items}">
	<render:item item="${item}"/>
</g:each>
<g:if test="${!items}">
	<span><g:message code="dashboard.noResults"/></span>
	<g:if test="${params.className}">
		<script>
			$(function() {
				$('#${params.className}Button').fadeOut();
			});
		</script>
	</g:if>
</g:if>
<script>
	$(function() {
		<g:if test="${items}">
			lastShowedItem = ${items[0].id};
		</g:if>
		${params.className?:'activity'}Offset = ${offset};
	});
</script>
