<aside id="sidebarHOF" class="well span12">
<h2>
	<g:message code="conference.hallOfFame.title"/> <a href="javascript:void(0);" onclick="Dashboard.updateHallOfFame('${hallOfFameUrl}');">
		<g:img dir="images" file="reload.png"/>
	</a> 
	<h5><g:message code="conference.hallOfFame.subtitle"/></h5>
	
</h2>
<g:each in="${activeTwitterUsers}" var="user">
	<g:link url="http://twitter.com/${user.tweetId}"><img title="${user.nTweets} tweets!" src="http://api.twitter.com/1/users/profile_image?screen_name=${user.tweetId}"/></g:link>
</g:each>
</aside>