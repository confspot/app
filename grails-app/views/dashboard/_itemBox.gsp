<aside id="sidebar" class="well marginRight10 marginLeft0">
	<span id="${className}Loader" style="display:none;">
		<g:img dir="images" file="loader.gif"/>
	</span>
	<div class="categoryContainer">
		<h2>
			<g:link mapping="conferenceDetailItem"
				params="['slug':conference.slug, 'type':className.toLowerCase()]" class="dashboardLink">
				<g:img class="dashboardIcon" file="${className}-icon.png" />
				<g:message code="dashboard.${className}" />
			</g:link>
		</h2>
		<g:if test="${items}">
			<g:each var="item" in="${items}">
				<render:item item="${item}"/>
			</g:each>
		</g:if>
		<g:else>
			<div id="${className}Content">
			</div>
			<g:set var="activityUrl" value="${g.createLink(controller:'dashboard',action:'activityStream',id:params.id, params:[className:className])}"/>
			<a id="${className}Button" href="javascript:void(0);" class="btn btn-primary btn-small" onclick="javascript:Dashboard.fetchStream('${activityUrl}',${className}Offset,'${className}Content')">
				<g:message code="dashboard.loadNext"/>
			</a>
			<script>
				var ${className}Offset = 1;
				$(function() {
					Dashboard.fetchStream('${activityUrl}',0,'${className}Content');
				});
			</script>
		</g:else>
	</div>
</aside>
