<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
        <title>${conference.title}</title>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		<r:require module="dashboard"/>
	</head>
	<body>
		<div style="clear:both;">&nbsp;</div>
		<g:if test="${!items}">
			<g:set var="hallOfFameUrl" value="${g.createLink(action:'checkForMostActiveUsers',controller:'dashboard',id:conference.id)}"/>
			<div id="hallOfFame" class="span12">
				<tmpl:hallOfFame hallOfFameUrl="${hallOfFameUrl}"/>
			</div>
			<script>
				$(function() {
					Dashboard.updateHallOfFame('${hallOfFameUrl}');
				});
			</script>

			<div class="span4">
				<aside id="sidebar" class="well marginRight10">
					<span id="activityContentLoader" style="display:none;">
						<g:img dir="images" file="loader.gif"/>
					</span>
					<g:set var="updatesUrl" value="${g.createLink(action:'checkForUpdates',controller:'dashboard',id:params.id)}"/>
					<h2>
						<g:message code="dashboard.activityStreamTitle"/>
						<a href="javascript:void(0);" onclick="Dashboard.updateActivityStream('${updatesUrl}');">
							<g:img dir="images" file="reload.png"/>
						</a>
					</h2>
					<div id="activityContent">
					</div>
					<g:set var="activityUrl" value="${g.createLink(controller:'dashboard',action:'activityStream',id:params.id)}"/>
					<script>
						var activityOffset = 1;
						var lastShowedItem;
						$(function() {
							Dashboard.fetchStream('${activityUrl}',0,'activityContent', function() {
								setTimeout(function() {
									Dashboard.updateActivityStream('${updatesUrl}');
								},60000);
							})
						});
					</script>
				</aside>
			</div>
			<div class="span8 marginLeft0">
				<g:each var="className" in="${['Tweet','Video','Presentation','Picture']}">
					<div class="span6 marginLeft0">
						<tmpl:itemBox className="${className}"/>
					</div>
				</g:each>
			</div>
		</g:if>
		<g:else>
			<div class="searchResults align-center" >
					<h1 id="jumbotron"><g:message code="dashboard.searchResults"/></h1>
			</div>
			<div class="span8 marginLeft0">
				<g:each var="mapEntry" in="${items}">
					<div class="span6 marginLeft0">
						<tmpl:itemBox className="${mapEntry.key}" items="${mapEntry.value}"/>
					</div>
				</g:each>
			</div>
		</g:else>
	</body>
</html>
