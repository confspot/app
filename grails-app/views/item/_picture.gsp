<div class="picture">
	<p><a href="${item.url}" target="new window"><img src="${item.url}" /></a></p>
	<legend>${item.title}</legend>
	<p><g:formatDate format="MM/dd/yy" date="${item.created}"/></p>
</div>
