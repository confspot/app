<div class="tweet ">
	<legend>
		<a href="https://twitter.com/${item.author}" target="_blank"><img src="http://api.twitter.com/1/users/profile_image?screen_name=${item.author}"/> <dl class="headerTweet"><dd>${item.realName}</dd><dt> @${item.author}</dt></dl></legend></a>
	<p>${item.title}</p>
	<p><g:formatDate format="${message(code:'default.date.format')}" date="${item.created}"/></p>


	<a href="${item.url}" target="new window"><g:message code="dashboard.goToTweet" /> </a>
	-
	<a class="tweetAction" href="https://twitter.com/intent/tweet?in_reply_to=${item.tweetId}">Reply</a>
	-:
	<a class="tweetAction" href="https://twitter.com/intent/retweet?tweet_id=${item.tweetId}">Retweet</a>
	-
	<a class="tweetAction" href="https://twitter.com/intent/favorite?tweet_id=${item.tweetId}">Favorite</a>
</div>
