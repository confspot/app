<div class="presentation">
	<legend>${item.title}</legend>
	<p><a href="${render.slideshareUrl(url:item.url)}" target="new window"><img src="${item.thumbUrl}"/></a></p>
	<p><g:formatDate format="MM/dd/yy" date="${item.created}"/></p>
</div>
