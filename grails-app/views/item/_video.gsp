<div class="video">
	<legend>${item.title.trimTo(30)} </legend>
	<a href="${item.url}" target="new window"><img src="${item.thumbPicUrl}"/></a>
	<p>${item.description.trimTo(100)}</p>
	<p>(${item.author})</p>
	<p><g:formatDate format="MM/dd/yy" date="${item.created}"/></p>
</div>
