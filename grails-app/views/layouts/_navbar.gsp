<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <g:link uri="/" class="brand logo">
            <r:img file="confspot.png" />
            ConfSpot.com
        </g:link>
        <div class="nav-collapse">
            <ul class="nav">
                <li><g:link controller="conference" action="create"><g:message code="layout.signUpYourConference" /></g:link></li>
            </ul>

            <%-- Search box --%>
			<form id="search-box" class="navbar-search pull-left" action="${createLink(mapping:'searchConferences')}" method="get">
				<input type="text" name="text" class="search-query span2" placeholder="${message(code:'conference.search')}">
			</form>
            
            <ul class="nav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    	<span id="specificCountry" style="display:${session.position?.country?.name ? 'inline' : 'none'}">
	                    	<g:message code="layout.main.location"/> <span id="countryName">${session.position?.country?.name}</span>
	                    </span>
	                    <span id="allCountries" style="display:${!(session.position?.country?.name) ? 'inline' : 'none'}">
	                    	<g:message code="layout.main.allEvents" />
	                    </span>
                   	</a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <g:message code="lang" />
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <render:langSwitch lang="es"/>
                        </li>
                        <li>
                            <render:langSwitch lang="en"/>
                        </li>
                    </ul>

                </li>
                <sec:ifNotLoggedIn>
                    <li>
                        <g:link controller="login" action="twitterAuth">
                            <i class="icon-twitter icon-white" data-original-title="icon-twitter" style="font-size: 18px;"></i>
                            <g:message code="layout.main.login" />
                        </g:link>
                    </li>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <li><g:link mapping="myConferences"><img src="${session.profileImage}" /> <sec:username/></g:link></li>
                    <li>
                        <g:link controller="logout">
                            <g:message code="layout.main.logout" />
                        </g:link>
                    </li>
                </sec:ifLoggedIn>
            </ul>
        </div>
    </div>
</div>
<div class="navbar-spacer"></div>
