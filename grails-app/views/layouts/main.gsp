<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="ConfSpot"/></title>
        <link rel="shortcut icon" href="${resource(file: '/images/favicon.ico')}">
		<g:layoutHead/>
		<r:require module="core"/>
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <r:layoutResources />
	</head>
	<body>
        <g:render template="/layouts/navbar" />
		<g:layoutBody/>
        <g:render template="/layouts/footer" />
        <r:layoutResources />
	</body>
</html>
