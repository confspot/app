<footer>
    <div class="container">
        <div class="row">
            <div class="span3">
                <h4>ConfSpot.com</h4>
                <ul>
                    <li><g:link uri="/"><g:message code="layout.home" /></g:link></li>
                    <li><g:link controller="conference" action="create"><g:message code="layout.signUpYourConference" /></g:link></li>
               </ul>
            </div>
            <div class="span3">
                <h4><g:message code="footer.contactUs" /></h4>
                <address>
                    <strong><g:message code="footer.confSpotTeam" /></strong><br>
                    <g:message code="footer.confSpotAddress" /><br/>
                    <a href="mailto:grails48@salenda.es">grails48@salenda.es</a>
                </address>
            </div>
            <div class="span4">
                <h4><g:message code="footer.aboutConfSpot" /></h4>
                <p><g:message code="footer.aboutConfSpot.text" /></p>
            </div>
            <div class="span2 social">
                <h4>Social</h4>
                <div class="row-fluid">
                    <div class="pull-left"><a class="btn btn-small" href="http://twitter.com/confspot"><i class="icon-large icon-twitter"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</footer>

<%@page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<g:if test="${RequestContextUtils.getLocale(request).language != 'en'}">
    <script type="text/javascript" src="${resource(plugin: 'jquery-ui', file: "js/jquery/i18n/jquery.ui.datepicker-${RequestContextUtils.getLocale(request).language}.js")}"></script>
</g:if>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36252382-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>