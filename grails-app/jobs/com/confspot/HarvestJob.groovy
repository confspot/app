package com.confspot

class HarvestJob {

    def youtubeService
    def twitterService
    def slideshareService
    def instagramService

	static triggers = {
		// Every 10 minutes
		simple name: 'harvest', startDelay:30000, repeatInterval: 600000

		// Every minute (only for testing)
        	//cron name: 'harvest', cronExpression: "0 * * ? * *"
	}

    def execute() {
		try {
	        	youtubeService.harvest()
		} catch (Exception e) {
			println "Exception ${e.message}"
		}
	
		try {
	        	slideshareService.harvest()
		} catch (Exception e) {
			println "Exception ${e.message}"
		}
	
		try {
	        	instagramService.harvest()
		} catch (Exception e) {
			println "Exception ${e.message}"
		}
	
		try {
			twitterService.harvest()
		} catch (Exception e) {
			println "Exception ${e.message}"
		}
    }
}
