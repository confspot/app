
import com.confspot.*

class BootStrap {

    def fixtureLoader
    def twitterService

    def init = { servletContext ->
        environments {
            production {
//				addIntroElements()
            }

            development {
				addIntroElements()
        	}
			
			String.metaClass.trimTo { to ->
				delegate.length()>to?("${delegate[0..(to-3)]}..."):delegate
			}
		}
	}
	
    def destroy = {
    }
	
	def addIntroElements() {
		def fixture = fixtureLoader.load('usersRoles')

		def grails48 = new Conference(
			title: "Grails 48",
			begin: new Date()-2,
			end: new Date(),
			localityName: "Leganes",
			countryName: "Spain",
			countryCode: "ES",
			lat:40.1d,
			lng:-3.2d,
			urlSite: "http://www.grails48.com",
			hashtag:"grails48",
			organizer:fixture.organizerConfspot,
			photo: "grails48"
		).save(flush:true)
//		fixture.organizerConfspot.addToConferences(grails48).save(flush:true)
				
		def confGr8 = new Conference(
			title:"GR8Conf",
			begin: new Date() + 5,
			end:  new Date() + 10,
			localityName: "Copenhagen",
			countryName: "Denmark",
			countryCode: "DK",
			lat:40.1d,
			lng:-3.2d,
			urlSite: "http://gr8conf.org",
			hashtag: "gr8conf",
			organizer:fixture.organizerGR8Conf,
			photo: "gr8",
		).save(flush:true)
//		fixture.organizerGR8Conf.addToConferences(confGr8).save(flush:true)
	
		def ggx = new Conference(
			title: "Groovy & Grails Exchange 2012",
			begin: new Date() + 30,
			end:  new Date() + 35,
			localityName: "London",
			countryName: "United Kingdom",
			countryCode: "GB",
			lat:51.507d,
			lng:-0.1276d,
			urlSite: "http://conferencia2012.agile-spain.org/",
			hashtag: "ggx",
			organizer:fixture.organizerGGX2012,
			photo: "ggx"
		).save(flush:true)
//		fixture.organizerGGX2012.addToConferences(ggx).save(flush:true)
	
		def springone = new Conference(
			title: "Springone 2GX",
			begin: new Date() - 60,
			end:  new Date() - 55,
			localityName: "Washington",
			countryName: "United States",
			countryCode: "US",
			lat:38.895d,
			lng:-77.036d,
			urlSite: "http://www.springone2gx.com/conference/washington/2012/10/home",
			hashtag: "springone",
			organizer:fixture.organizerSpringone,
			photo: "springone"
		).save(flush:true)
//		fixture.organizerSpringone.addToConferences(springone).save(flush:true)
	
		def cas = new Conference(
			title: "CAS 2012",
			begin: new Date() - 20,
			end:  new Date() - 18,
			localityName: "Caceres",
			countryName: "Spain",
			countryCode: "ES",
			lat:39.476d,
			lng:-6.3707d,
			urlSite: "http://conferencia2012.agile-spain.org",
			hashtag: "cas2012",
			organizer:fixture.organizerCAS,
			photo: "cas"
		).save(flush:true)
//		fixture.organizerCAS.addToConferences(cas).save(flush:true)
	}
}
