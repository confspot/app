dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}

def envVar = System.env.VCAP_SERVICES
def credentials = envVar?grails.converters.JSON.parse(envVar)["postgresql-9.1"][0]["credentials"]:null

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
	        driverClassName = "org.postgresql.Driver"
     	    url =  "jdbc:postgresql://${credentials?.hostname}:${credentials?.port}/${credentials?.name}"
            username = credentials?.username
            password = credentials?.password
        }
    }
}
