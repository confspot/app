modules = {
	
	core {
        dependsOn 'jquery, jquery-ui, bootstrap-js, bootstrap-responsive-css'

        /* Global CSS */
		resource url:'css/distinctive.css'
		resource url:'css/main.css'

        /* Global JS */
		resource url:'js/main.js', disposition: 'defer'
	}

    'google-maps' {
        resource url:'/js/lib/googlemaps.js', linkOverride: 'http://maps.google.com/maps/api/js?sensor=true', disposition: 'head', attrs:[type: 'js']
    }

    gmaps {
        dependsOn 'google-maps'
        resource url:'/js/lib/gmaps.js', disposition: 'head'
    }


    geolocation {
		dependsOn 'gmaps'
		resource url:'js/geolocation/main.js', disposition: 'head'
		resource url:'css/geolocation/main.css', disposition: 'head'
	}
	
	shareGeolocation {
		resource url:'js/geolocation/share.js', disposition: 'head'
	}

	createConference {
		dependsOn 'conference'
		resource url:'js/conference/create.js'
	}

	conference {
		resource url:'js/conference/jquery.appear-1.1.1.js'
		resource url:'css/conference/main.css'
		resource url:'js/conference/main.js'
	}
	
	myconferences {
		resource url:'css/conference/main.css'
	}

	dashboard{
		resource url:'js/dashboard/dashboard.js'
		resource url:'css/dashboard/dashboard.css'
	}
	
	masonry{
		dependsOn 'jquery'
		resource url:'js/lib/jquery.masonry.min.js'
	}
}
