class UrlMappings {

	static mappings = {

		name root: "/" {controller = 'home'; action = [GET:'index'] }

		// Spring security. Do not change it!
		name loginIndex: "/login/index" {controller = 'login'; action = 'index'}
		name loginAuth: "/login/auth" {controller = 'login'; action = 'auth'}
		name loginAuthAjax: "/login/authAjax" {controller = 'login'; action = 'authAjax'}
		name loginDenied: "/login/denied" {controller = 'login'; action = 'denied'}
		name loginFull: "/login/full" {controller = 'login'; action = 'full'}
		name loginAuthFail: "/login/authfail" {controller = 'login'; action = 'authfail'}
		name loginAjaxSuccess: "/login/ajaxSuccess" {controller = 'login'; action = 'ajaxSuccess'}
		name loginAjaxDenied: "/login/ajaxDenied" {controller = 'login'; action = 'ajaxDenied'}
		name logoutIndex: "/logout/index" {controller = 'logout'; action = 'index'}

		// Search
		name searchInConference: "/conference/$slug/search" {controller = 'search'; action = [GET:'search'] }
		name searchConferences: "/conference/search" {controller = 'search'; action = [GET:'searchConferences'] }

		// Conference
		name showConference: "/conference/$slug" {controller = 'conference'; action = [GET:'show'] }
		name conferenceDetailItem: "/conference/$slug/$type" { controller = 'conference'; action = [GET:'detailItem']}
		name createConference: "/conference/create" {controller = 'conference'; action = [GET:'create'] }
		name editConference: "/conference/edit/$slug" {controller = 'conference'; action = [GET:'edit'] }
		name deleteConference: "/conference/delete/$slug" {controller = 'conference'; action = [GET:'delete'] }
		name saveConference: "/conference/save" { controller = 'conference'; action = [POST:'save'] ; params = params}
		name myConferences: "/conference/my" {controller = 'conference'; action = [GET:'myConferences']}

		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"500"(view:'/500')
        "404"(view: '/404')
	}
}
