import com.confspot.TwitterUserDetailsService

// Place your Spring DSL code here
beans = {
    userDetailsService(TwitterUserDetailsService){
        grailsApplication = ref('grailsApplication')
    }
}
