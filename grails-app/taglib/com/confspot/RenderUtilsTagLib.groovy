package com.confspot

import org.springframework.web.servlet.support.RequestContextUtils

class RenderUtilsTagLib {

	static namespace = "render"

	/*
	 * <render:item item="${item}"/>
	 */
	def item = { attrs -> 
		//Get the name of the Item class
		def unqualifiedClassName = (attrs.item.class.name =~ /.*\.(.*)/)[0][1]
		//Uncapitalize
		unqualifiedClassName = "${unqualifiedClassName[0].toLowerCase()}${unqualifiedClassName[1..-1]}"
		//Render its template
		out << render(template:"/item/$unqualifiedClassName", model:[item:attrs.item])
	}

	def slideshareUrl = { attrs ->
		out << "http://www.slideshare.net$attrs.url"
	}

    def langSwitch = {attrs,body ->
        def lang = attrs.remove('lang')
        def url = request.forwardURI + "?"
        def requestParams = [:]
        params.each {k,v ->
            if (!k in ['lang', 'action', 'controller']) requestParams[k] = v
        }
        requestParams.each {k,v->
            url += "$k=$v&"
        }
        if (lang != 'en') {
            url += "lang=en"
            out << "<a href=\"$url\">"
            out << r.img(file: 'gb.png')
            out << "&nbsp;"
            out << message(code: 'lang.en')
            out << "</a>"
        } else {
            url += "lang=es"
            out << "<a href=\"$url\">"
            out << r.img(file: 'es.png')
            out << "&nbsp;"
            out << message(code: 'lang.es')
            out << "</a>"
        }
    }
}
