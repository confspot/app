package com.confspot

class BootstrapTagLib {
	static namespace = "bootstrap"
	
	def alert = { attrs, body ->
			out << """
            <div class=\"alert alert-${attrs.type}\">
               """
			if(attrs.close){
				out << """<a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>"""
			}                 
			out << """<strong>${attrs.title}</strong>"""
			out << body()
			out << "</div>"
	}
}
