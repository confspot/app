package com.confspot;

class GeolocationTagLib {
	static namespace = "geolocation"
	def locateMe =  {attrs, body ->
	  out << render(template:"../templates/geolocation",model:[attrs:attrs, body:body])
	}

	def country = {attrs, body ->
		out << session.country ?: message(code:'all')
	}
}
