package com.confspot

class SearchService {

	/**
	 * Search conferences
	 *
	 * @param text The text to search in conferences attributes
	 *
	 * @return A list of conferences
	 */
	public List<Conference> searchConferences(String text) {

		println text

		def conferences = Conference.withCriteria {
			or {
				ilike 'title', '%' + text + '%'
				ilike 'localityName', '%' + text + '%'
				ilike 'countryName', '%' + text + '%'
				ilike 'urlSite', '%' + text + '%'
				ilike 'hashtag', '%' + text + '%'
			}

			order 'begin', 'desc'
		}

		return conferences
	}

	/**
	 * List the items related to a conference by type
	 *
	 * @param conference The conference
	 * @param type The item type to filter
	 *
	 * @return A list of items
	 */
	public List<Item> listItemsByConferenceByType(Conference conference, String type, Integer offset = 0) {

		def className = null
		if (type == 'tweet') {
			className = Tweet.class
		} else if (type == 'video') {
			className = Video.class
		} else if (type == 'presentation') {
			className = Presentation.class
		} else if (type == 'picture') {
			className = Picture.class
		}

		if (!className) {
			return []
		}

		def items = className.createCriteria().list() {
			conferences {
				eq 'id', conference.id
			}

			order 'created', 'desc'

			maxResults 10
			firstResult offset
		}

		return items
	}
	
	/**
	 * 
	 * @param conference
	 * @return
	 */
	def searchMostActiveTwitterUsers(conference){
		def activeTwitterUsers = []
		def c = Tweet.createCriteria()
		
		def results = c.list {
			projections {
				groupProperty "author"
				count "author", 'nTweets' //Implicit alias is created here !
			}
			conferences {
				eq ('id',conference.id)
			}
			order 'nTweets', 'desc'
			maxResults 20
		}
		results.each{
			activeTwitterUsers << ['tweetId':it[0],'nTweets':it[1]]
		}
		activeTwitterUsers
	}
	
}
