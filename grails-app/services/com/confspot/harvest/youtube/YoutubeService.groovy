package com.confspot.harvest.youtube

import groovy.json.JsonSlurper
import com.confspot.Video
import com.confspot.Conference
import com.confspot.Item
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.List;

import com.confspot.harvest.HarvesterService

class YoutubeService extends HarvesterService {

    /**
     * Get all the videos (max=50) from youtube related to the query
     *
     * @param query The search query
     *
     * @return the list of videos
     */
	protected List<Item> getExternalItems(String query) {
		def slurper = new JsonSlurper()

		def list = []
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

		try {
			def serviceUrl = "http://gdata.youtube.com/feeds/api/videos?q=${query}&start-index=1&max-results=50&v=2&alt=json"
			def content = new URL(serviceUrl).getText("UTF-8")
			def response = slurper.parseText(content)

			response.feed.entry.each { video ->
				def map = [:]

				map.youtubeId = video.id."\$t"
				map.title = video.title."\$t"
				map.url = video.content.src
				map.description = video."media\$group"."media\$description"."\$t"

				def thumbs = video."media\$group"."media\$thumbnail"
				map.thumbPicUrl = thumbs.find { it."yt\$name" == 'hqdefault'}.url

				map.author = video.author.name."\$t"[0]
				Integer viewCount = video."yt\$statistics"?.viewCount as Integer
				map.viewCount = viewCount ?: 0

				Date dateCreated = df.parse(video.published."\$t")
				map.created = dateCreated ?: new Date()

				list << map
			}
		} catch (Exception e) {
			log.error "There was an error with youtube"
			log.error e.getMessage()
		}

		return list
    }

	protected Item findElement(Map externalItem) {
		return Video.findByYoutubeId(externalItem.youtubeId)
	}

    protected Item createElement(Map params) {
    	return new Video(params)
    }

    protected Item updateElement(Item item, Map params) {
    	item.viewCount = params.viewCount
    	return item
    }

}
