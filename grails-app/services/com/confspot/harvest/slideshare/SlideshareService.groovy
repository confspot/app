package com.confspot.harvest.slideshare

import groovy.json.JsonSlurper
import com.confspot.Presentation
import com.confspot.Item
import com.confspot.Conference
import java.text.DateFormat
import java.text.SimpleDateFormat

import com.confspot.harvest.HarvesterService

class SlideshareService extends HarvesterService {

    /**
     * Get all presentations from slideshare (with pagination)
     *
     * @param query The search query
     *
     * @return the list of presentations
     */
    protected List<String> getExternalItems(String query) {

    	def list = []

    	Integer page = 1
    	Boolean finished = false
    	while (!finished) {
    		def tmpList = searchWithPagination(query, page)

    		if (tmpList.size() == 0) {
    			finished = true
    		} else {
    			list.addAll(tmpList)
    			page++
    		}
    	}

    	return list
    }


    /**
     * Search in slideshare only in the selected page
     *
     * @param query The text to find
     * @param page The page
     *
     * @return the list of presentations for the current text and page
     */
    private searchWithPagination(String query, Integer page) {

		def slurper = new JsonSlurper()

		def list = []
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    	try {
			def apiUrl = "http://www.slideshare.net/search/slideshow.json?q=${query}&page=${page}"
			def content = new URL(apiUrl).getText("UTF-8")
			def response = slurper.parseText(content)

			response.search_results.each { doc ->
				def map = [:]

				map.slideshareId = doc.id
				// Ugly but...
				map.title = doc.title.replaceAll("<strong>", "").replaceAll("</strong>", "") 
				map.url = doc.url
				map.author = doc.user_login
				map.slides = doc.total_slides
                map.thumbUrl = doc.thumb_url.replaceAll("//", "http://")

				Date dateCreated = df.parse(doc.created_at)
				map.created = dateCreated ?: new Date()

				list << map
			}
		} catch (Exception e) {
			log.error "There was an error with slideshare"
			log.error e.getMessage()
		}

		return list
    }

	protected Item findElement(Map externalItem) {
		return Presentation.findBySlideshareId(externalItem.slideshareId)
	}

    protected Item createElement(Map params) {
		return new Presentation(params)
    }
}
