package com.confspot.harvest.instagram

import groovy.json.JsonSlurper
import com.confspot.Picture
import com.confspot.Conference
import com.confspot.Item

import com.confspot.harvest.HarvesterService

class InstagramService extends HarvesterService {

	def grailsApplication

    /**
     * Get all the pictures
     *
     * @param query The search query
     *
     * @return the list of pictures
     */
    protected List<String> getExternalItems(String query) {

    	def list = []

    	String serviceUrl = null
    	Boolean finished = false
    	while (!finished) {
    		def result = searchWithPagination(query, serviceUrl)

    		if (result.nextUrl) {
    			list.addAll(result.list)
    			serviceUrl = result.nextUrl
			} else {
				finished = true
    		}
    	}

    	return list
    }

    /**
     * Search in instagram with pagination
     *
     * @param query The tag to find
     * @param query nextUrl The url to search
     *
     * @return a Map with the nextUrl to search a the list of pictures
     */
    private Map searchWithPagination(String query, String nextUrl = null) {
        def clientId = grailsApplication.config.instagram.clientId

		def slurper = new JsonSlurper()

		def list = []
		def result = [:]
		
		try {
			def serviceUrl = nextUrl ?: "https://api.instagram.com/v1/tags/${query}/media/recent?client_id=${clientId}"

			def content = new URL(serviceUrl).getText("UTF-8")
			def response = slurper.parseText(content)

			result.nextUrl = response.pagination.next_url

			response.data.each { picture ->
				def map = [:]

				map.instagramId = picture.id
				map.title = picture.caption?.text ?: 'No title' // TODO Fix me
				map.url = picture.images.standard_resolution.url
				map.author = picture.caption?.from?.username ?: 'Unknown' // TODO Explore this

				Long time = picture.created_time as Long
				map.created = new Date(time*1000)
				list << map
			}
		} catch (Exception e) {
			log.error "There was an error with instragram"
			log.error e.getMessage()
		}

		result.list = list

		return result
    }

	protected Item findElement(Map externalItem) {
		return Picture.findByInstagramId(externalItem.instagramId)
	}

    protected Item createElement(Map params) {
    	return new Picture(params)
    }
}
