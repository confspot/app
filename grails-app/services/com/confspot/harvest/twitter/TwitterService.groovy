package com.confspot.harvest.twitter

import javax.annotation.PostConstruct

import twitter4j.Status
import com.confspot.*

class TwitterService {

	// Injected by grails
	def grailsApplication

	// The object to configure the searchings
	StreamingTwitterHarvester harvester
	
	@PostConstruct
	private void init() {
		harvester = new StreamingTwitterHarvester(grailsApplication:grailsApplication)
	}

	def harvest() {
		def confs = Conference.list()
		def hashtags = confs.hashtag.collect{it.startsWith('#') ? it : "#$it"}
		
		harvester.restart(hashtags) { tweetJson ->
			def tweet = saveTweet(tweetJson)
		}
	}

	/**
	 * It saves the new tweet harvested and it returns it.
	 */
	def saveTweet(Status status) {
        def tweet = new Tweet()

        Item.withNewSession {
            def confs = Conference.list()

			tweet.tweetId = status.id
			tweet.retweet = status.retweetCount
	
			tweet.realName = status.user.name
			tweet.author = status.user.screenName
			tweet.created = status.createdAt
			tweet.url = "http://twitter.com/status/${status.user.screenName}/${status.id}"
			tweet.title = status.text

			confs.findAll { conf -> status.text.contains("#${conf.hashtag}") }.each { conf ->
				tweet.addToConferences(conf)
				conf.addToItems(tweet)
				conf.save(flush:true)
			}

			try {
				tweet.save(failOnError:true)
			} catch (Exception e) {
				log.error(e.message)
			}
		}
		
		return tweet
	}
}
