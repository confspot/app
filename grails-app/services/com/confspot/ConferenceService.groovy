package com.confspot

class ConferenceService {
	
	def grailsApplication
	
	def findRelevantConferences(position) {
		def result
		
		result = Conference.createCriteria().list {
			if(position) {
				eq('countryCode', position.country.code)
			}

			order('begin', 'desc')
			maxResults(grailsApplication.config.com.confspot.home.maxItems)
		}
		
		if(position && result.size() < grailsApplication.config.com.confspot.home.maxItems) {
			result += Conference.createCriteria().list {
				order('begin', 'desc')
				maxResults(grailsApplication.config.com.confspot.home.maxItems)
			}
		}
		
		result.unique{it.id}
		result = result.sort{it.begin}
		
		return result
	}
}
