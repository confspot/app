package com.confspot

import org.grails.plugin.geolocation.GeoPosition

import grails.converters.JSON

class LocationService {
	
	def getLocationInfoByGeoPosition(position, locale) {
		return position ? getLocationInfoByGeoPosition(position.coords.latitude, position.coords.longitude, locale) : null
	}
	
	def getLocationInfoByGeoPosition(latitude, longitude, locale) {
		def result = [:]
		def countryName, countryCode, localityName, postalCode
		def url = new URL ("http://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&sensor=true&language=${locale.language}")
		
		if(latitude && longitude) {
			def res = JSON.parse(url.newReader())
			if(res.status == "OK"){
				res.results.address_components.each { component ->
					def country = component.find{it.types.contains("country")}
					def locality = component.find{it.types.contains("locality")}
					def postalCodeElement = component.find{it.types.contains("postal_code")}
					
					countryName = countryName ?: country?.long_name
					countryCode = countryCode ?: country?.short_name
					localityName = localityName ?: locality?.long_name
					postalCode = postalCode ?: postalCodeElement?.long_name
				}
			}
			
			result.country = [name:countryName, code:countryCode]
			result.postalCode = postalCode
			result.localityName = localityName
		}
		
		return result
	}
}
