package com.confspot

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class User {

	transient springSecurityService

	String username

    //Actually we don't care about passwords
    String password

    String twitterId
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static constraints = {
		username blank: false, unique: true
        password nullable: true
	}

    static mapping = {
        table 'app_user'
    }

    Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

}
