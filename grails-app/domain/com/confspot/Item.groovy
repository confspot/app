package com.confspot

class Item {
	
	String author
	Date created
	String url
	String title
	
	static belongsTo = Conference
	static hasMany = [conferences:Conference]
	
	static mapping = {
		tablePerHierarchy false
		title type:"text"
		url type:"text"
	}
	
    static constraints = {
    }
}
