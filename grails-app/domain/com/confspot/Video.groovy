package com.confspot

class Video extends Item {
	
	String youtubeId
	String description
	Integer viewCount = 0
	String thumbPicUrl

    static constraints = {
    	description blank:true
    }

	static transients = ['videoId']

	static mapping = {
		title type:"text"
		description type:"text"
	}

	public String getVideoId() {
		def regexp = /.*video:(.*)/
		(this.youtubeId ==~ regexp)?(this.youtubeId =~ regexp)[0][1]:''
	}
}
