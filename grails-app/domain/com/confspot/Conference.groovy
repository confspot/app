package com.confspot

class Conference {

	transient slugGeneratorService
	
	String title
	Date begin
	Date end
	String localityName
	String countryCode
	String countryName
	String urlSite
	Double lat
	Double lng
	String postalCode
	User organizer
	String slug = ""
	
	String hashtag
	String photo = "${new Random().nextInt(3)}"
	
	static hasMany = [items: Item]

    static constraints = {
		urlSite url: true
		hashtag unique: true
		postalCode nullable:true
		end nullable:false
		begin nullable:false, validator: {val,obj->  if(val > obj.end) "survey.begin.error.beginGreaterThanEnd"}
    }

    static mapping = {
        begin column: 'date_begin'
        end column: 'date_end'
    }

	def beforeInsert() {
        this.slug = slugGeneratorService.generateSlug(this.class, "slug", title)
    }

    def beforeUpdate() {
        if (isDirty('title')) {
            this.slug = slugGeneratorService.generateSlug(this.class, "slug", title)
        }
    }
}
