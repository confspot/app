package com.confspot

import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class SearchController {

	def searchService

	/**
	 * Search info about a conference
	 *
	 * @param slug The slug of the conference
	 * @param text The text to search in all the items related to the conference
	 */
	def search(String slug, String text) {

		def conference = Conference.findBySlug(slug)
		if (!conference) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
			return redirect(mapping:"loginAuth")
		}

		if (!text || !text?.trim()) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
			return redirect(mapping:"loginAuth")
		}

		def items = Item.withCriteria {
			conferences {
				eq('id',conference.id)
			}
			ilike('title',"%${text.trim()}%")
		}.groupBy { (it.class.name =~ /.*\.(.*)/)[0][1] }

		render view:'/conference/show', model:[conference: conference, items:items]
	}

	/**
	 * Search conferences
	 *
	 * @param text The text to search
	 */
	def searchConferences(String text) {

		if (!text || !text?.trim()) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
			return redirect(mapping:"loginAuth")
		}

		def conferences = searchService.searchConferences(text.trim())

		render view:'/conference/searchResult', model:[conferences:conferences]
	}
}
