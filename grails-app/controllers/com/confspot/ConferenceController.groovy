package com.confspot

import grails.plugins.springsecurity.Secured

class ConferenceController {

    def springSecurityService
	def locationService
	def searchService
	
	static defaultAction = 'myConferences'

	@Secured(['ROLE_ORGANIZER', 'ROLE_ADMIN'])
	def myConferences(){
		def organizer = springSecurityService.currentUser
		def conferences = organizer ? Conference.findAllByOrganizer(organizer) : []
		[conferences:conferences]
	}


    @Secured(['ROLE_ORGANIZER', 'ROLE_ADMIN'])
    def create() {
        [conference: new Conference(params)]
    }
	
	@Secured(['ROLE_ORGANIZER', 'ROLE_ADMIN'])
	def edit(String slug) {
		def conference = Conference.findBySlug(slug)
		if (!conference) {
			flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
			redirect(mapping: "myConferences")
		}else {
            if (springSecurityService.currentUser == conference.organizer) {
				render view: "create", model: [conference:conference]
			}else{
				flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.delete.no.organizer.error"), type: 'error')
				redirect(mapping: "showConference", params:[slug:conference.slug])
			}
		}
	}

	@Secured(['ROLE_ORGANIZER', 'ROLE_ADMIN'])
    def save() {
        def conference = params.id ? Conference.get(params.id) : new Conference()
		conference.properties = params['title','siteUrl','hashtag','lat','lng']
		if (params.id) conference.id = params.id.toLong()
		def locationInfo = locationService.getLocationInfoByGeoPosition(params.lat, params.lng, request.locale)
		if(!locationInfo) {
			flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.save.error.location"), type: 'error')
			render(view: "create", model:[conference:conference])
		} else {
			conference.countryCode = locationInfo.country.code
			conference.countryName = locationInfo.country.name // TODO Don't save the i18n name, and create it in each request from the code and the user locale
			conference.localityName = locationInfo.localityName
			conference.postalCode = locationInfo.postalCode
			conference.end = params.date('end', message(code:'locale.format.date.java'))
			conference.begin = params.date('begin', message(code:'locale.format.date.java'))
			conference.organizer = springSecurityService.currentUser
			
	        if (conference.save(flush:true)) {
				flash.message = new Message(title: message(code: "global.success"), body: message(code: "conference.save.success"), type: 'success')
	            redirect(mapping:'showConference', params:[slug:conference.slug])
	        } else {
	        	flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.save.error"), type: 'error')
	            render(view: "create", model:[conference:conference])
	        }
		}
    }
	
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(String slug) {
        def conference = Conference.findBySlug(slug)
        if (!conference) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
            redirect(mapping: "myConferences")
        }else {
			[conference: conference]
        }
    }
    
	@Secured(['ROLE_ORGANIZER', 'ROLE_ADMIN'])
    def delete(String slug) {
		def conference = Conference.findBySlug(slug)
		if (!conference) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
        }else {
			if (springSecurityService.currentUser == conference.organizer) {
	            conference.delete(flush:true)
	            flash.message = new Message(title: message(code: "global.success"), body: message(code: "conference.delete.success"), type: 'success')
	        }else{
				flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.delete.no.organizer.error"), type: 'error')
			}
		}
		redirect(mapping: "myConferences")
    }

    /**
     * Show the items related to the conference by type
     *
     * @param slug The slug of the conference
     * @param type One of the following values: tweet, video, presentation, picture
     * @param nItems The number of items previously loaded (for pagination calls)
     */
    def detailItem(String slug, String type, Integer nItems) {

		def conference = Conference.findBySlug(slug)
		if (!conference) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.not.found.error"), type: 'error')
            return redirect(action: "index")
        }

		if (!type) {
            flash.message = new Message(title: message(code: "global.oops"), body: message(code: "conference.item.notFound"), type: 'error')
            return redirect(action: "index")
		}

		if (!nItems) {
            nItems = 0
        }

		def items = searchService.listItemsByConferenceByType(conference, type, nItems)

		if (request.xhr) {
			render template:'/conference/items', model:[conference:conference, items:items, type:type]	
		} else {
			render view:'/conference/conferenceByItem', model:[conference:conference, items:items, type:type]	
		}
		
    }
	
}
