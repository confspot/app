package com.confspot

import grails.converters.JSON

import org.grails.plugin.geolocation.*

class HomeController {
	
	def locationService
	def conferenceService
	
	def index() {
		def conferences = conferenceService.findRelevantConferences(session.position)
		return [conferences:conferences]
	}
	
	def setGeoPosition() {
		List result = []
		def o = JSON.parse(params.val); // Parse a JSON String
		def position = [timestamp:o.timestamp, coords:new Coordinates(o.coords)]
		
		if(position.coords.latitude && position.coords.longitude) {
			def locationInfo = locationService.getLocationInfoByGeoPosition(position, request.locale)
			position.putAll(locationInfo)
		}
		
		result << [
			lat:position.coords.latitude, 
			lng:position.coords.longitude, 
			countryName:position.country.name
		]
		
		session.position = position
		
		render result as JSON
	}
}
