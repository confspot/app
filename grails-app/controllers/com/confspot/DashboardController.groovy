package com.confspot

class DashboardController {

	
	def searchService
	
	def test() {
		HarvestJob.triggerNow()
/*		Conference.list().each { println it.dump() }
		def video = new Video(
			author: "olmaygti",
			created: new Date()+1,
			url: "https://www.youtube.com/watch?v=BObtYHq4cWo",
			title: "Title!",
			youtubeId: 'BObtYHq4cWo',
			description: 'test').save()

		def conf = Conference.get(1)

		println conf

		conf.addToItems(video)
		video.addToConferences(conf)
		conf.save()

		println conf.items
		println video
		println conf.errors*/
	}
	
	def dashboard() { 
		if(params.id) {
			[conference:Conference.get(params.id as long)]
		}
	}

	def activityStream() {
		def maxItems = grailsApplication.config.com.confspot.dashboard[params.className?'maxItems':'maxStreamItems']
		if(params.id) {
			def classToQuery = params.className?grailsApplication.getClassForName("com.confspot.${params.className}"):Item
			[items:classToQuery.withCriteria {
				conferences {
					eq('id',Conference.get(params.id as long)?.id)
				}
				maxResults maxItems
				firstResult params.offset?(params.offset as int):(params.offset=0)
				order("created","desc")
				order("id","desc")
			},offset:(params.offset as int)+maxItems]
		}
	}

	def checkForUpdates() {
		if(params.objectId && params.id) {
			def lastShowedItem = Item.get(params.objectId as long)
			def lastItem = Item.createCriteria().get() {
				conferences {
					eq('id',Conference.get(params.id as long)?.id)
				}
				maxResults 1 
				order("id","desc")
			}
			//Assuming a greater ID means a newer item
			if(lastItem.id > lastShowedItem.id) {
				def newItems = Item.withCriteria {
					conferences {
						eq('id',Conference.get(params.id as long)?.id)
					}
					maxResults grailsApplication.config.com.confspot.dashboard.maxStreamItems
					order("id","desc")
					order("created","desc")
				}
				render(view:'activityStream',model:[items:newItems,offset:0])
			}else {
				render ''
			}
		}else { render '' }
	}
	
	def checkForMostActiveUsers(){
		def conference = Conference.get(params.id)
		def result = []
		if(conference) result = searchService.searchMostActiveTwitterUsers(conference)
		render template:'hallOfFame' , model:[activeTwitterUsers:result]
	}
	
}
