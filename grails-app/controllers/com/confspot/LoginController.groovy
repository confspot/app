package com.confspot

import org.springframework.security.web.savedrequest.DefaultSavedRequest
import twitter4j.TwitterFactory
import twitter4j.conf.ConfigurationBuilder
import twitter4j.Twitter
import twitter4j.ProfileImage

class LoginController {

    def springSecurityService
    def grailsApplication

    def twitterAuth = {
        def config = grailsApplication.config.twitter
        def configurationBuilder = new ConfigurationBuilder()
        configurationBuilder.with {
            debugEnabled = true
            OAuthConsumerKey = config.key
            OAuthConsumerSecret = config.secret
        }
        def twitterFactory = new TwitterFactory(configurationBuilder.build())
        def twitter = twitterFactory.getInstance()

        def requestToken = twitter.getOAuthRequestToken(createLink(action: 'twitterCallback', absolute: true))
        session.twitterToken = requestToken
        session.twitter = twitter
        redirect url: requestToken.authenticationURL

    }

    def twitterCallback = {
        def requestToken = session.twitterToken
        Twitter twitter = session.twitter

        def accessToken = twitter.getOAuthAccessToken(requestToken, params.oauth_verifier)

        def user = User.findByTwitterId(accessToken.userId)
        if (!user) {
            User.withTransaction {
                user = new User(username: accessToken.screenName, twitterId: accessToken.userId, enabled: true)
                user.save(failOnError: true)

                UserRole.create(user, Role.findByAuthority('ROLE_ORGANIZER'))
            }
        }

        def profileImage = twitter.getProfileImage(user.username, ProfileImage.MINI)
        session.profileImage = profileImage.URL

        springSecurityService.reauthenticate(user.username)

        DefaultSavedRequest url = session['SPRING_SECURITY_SAVED_REQUEST_KEY']
        if (url) {
            redirect url: url.redirectUrl
        } else {
            redirect uri: '/'
        }


    }
}
