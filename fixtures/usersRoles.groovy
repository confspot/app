import com.confspot.User
import com.confspot.Role
import com.confspot.UserRole

fixture {
	organizer(Role, authority: 'ROLE_ORGANIZER')
	admin(Role, authority: 'ROLE_ADMIN')
	user(Role, authority: 'ROLE_USER')
	
	organizerConfspot(User, username:"confspot", twitterId: '938635351', enabled: true)
	organizerGR8Conf(User, username:"sbglasius", twitterId: '19148763', enabled: true) //Søren Berg Glasius
	organizerGGX2012(User, username:"GGX", twitterId: '183207548', enabled: true) // ggx
	organizerSpringone(User, username:"springone2gx", twitterId: '48158548', enabled: true) //SpringOne 2GX 2012
	organizerCAS(User, username:"CAS2K12", twitterId: '595750499', enabled: true) // CAS2012
	
	organizerUserRole1(UserRole,user:organizerConfspot,role:organizer)
	organizerUserRole2(UserRole,user:organizerGR8Conf,role:organizer)
	organizerUserRole3(UserRole,user:organizerGGX2012,role:organizer)
	organizerUserRole4(UserRole,user:organizerSpringone,role:organizer)
	organizerUserRole5(UserRole,user:organizerCAS,role:organizer)
	
    adminUserRole(UserRole,user:organizerConfspot,role:admin)
}